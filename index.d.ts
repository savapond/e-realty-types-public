/*
    Type definitions for e-realty-back, e-realty-front
    Projects:
     * https://bitbucket.org/omworks/e-realty-back/
     * https://bitbucket.org/omworks/e-realty-front/
*/

export type RealtyClass = 'condo' |
    'residential' |
    'commercial' ;


export type RealtySortBy = 'createdOnAsc' |
    'createdOnDesc' |
    'updatedOnDesc' |
    'updatedOnAsc' |
    'listPriceAsc' |
    'listPriceDesc' |
    'random' |
    'soldDateAsc' |
    'soldDateDesc' |
    'soldPriceAsc' |
    'soldPriceDesc';

export type RealtyLastStatus = 'Sus' | 'Exp' | 'Sld' | 'Ter' | 'Dft' | 'Lsd' | 'Sc' | 'Lc' | 'Pc' | 'Ext' | 'New';

export interface RealtyAPICondominium {
    ammenities: null[];
    buildingInsurance: null;
    condoCorp: null;
    condoCorpNum: null;
    exposure: null;
    lockerNumber: string;
    locker: null;
    parkingType: null;
    pets: null;
    propertyMgr: string;
    stories: null;
    fees: {
        cableInlc: null;
        heatIncl: string;
        hydroIncl: string;
        maintenance: string;
        parkingIncl: null;
        taxesIncl: null;
        waterIncl: null;
    };
}

export interface RealtyAPIDetails {
    airConditioning: string;
    basement1: string;
    basement2: string;
    centralVac: null;
    den: null;
    description: string;
    elevator: null;
    exteriorConstruction1: string;
    exteriorConstruction2: null;
    extras: string;
    furnished: null;
    garage: null;
    heating: string;
    numBathrooms: string;
    numBathroomsPlus: string;
    numBedrooms: string;
    numBedroomsPlus: string;
    numFireplaces: string;
    numGarageSpaces: string;
    numParkingSpaces: string;
    numRooms: null;
    numRoomsPlus: null;
    patio: null;
    propertyType: string;
    sqft: string;
    style: string;
    swimmingPool: string;
    virtualTourUrl: null;
    yearBuilt: string;
}

export interface RealtyAPILot {
    acres: string;
    depth: string;
    irregular: null;
    legalDescription: null;
    measurement: null;
    width: string;
}

export interface RealtyAPIAddress {
    area: string;
    city: string;
    country: string;
    district: string;
    majorIntersection: string;
    neighborhood: string;
    streetDirection: string;
    streetName: string;
    streetNumber: string;
    streetSuffix: string;
    unitNumber: null;
    zip: string;
    state: string;
}

export interface RealtyAPIQueryParams {
    mlsNumber: string;
    area: string[];
    city: string[];
    neighborhood: string;
    minPrice: number;
    maxPrice: number;
    streetNumber: string;
    streetName: string;
    propertyType: string[];
    style: string[];
    minBeds: number;
    maxBeds: number;
    class: RealtyClass;
    listDate: string;
    updatedOn: string;

    // sortBy - default: 'createdOnDesc'
    sortBy: RealtySortBy;
    pageNum: number;
    resultsPerPage: number;
    type: 'sale' | 'lease';
    map: string;
    minBaths: number;
    maxBaths: number;
    boardId: number[];
    status: 'A' | 'U';
    lastStatus: RealtyLastStatus[];
    minSoldPrice: string;
    maxSoldPrice: string;
    minSoldDate: string;
    maxSoldDate: string;

    // operator - default: 'AND'
    operator: 'AND' | 'OR';

    // condition - default: 'EXACT'
    condition: 'EXACT' | 'CONTAINS';
    keywords: string;
    hasImages: boolean;
    displayAddressOnInternet: 'Y' | 'N';
    displayPublic: 'Y' | 'N';
    minSqft: number;
    minParkingSpaces: number;
}

export interface RealtyAPIMap {
    latitude: string;
    longitude: string;
}

export interface RealtyAPIOpenHouse {
    [key: number]: {
        date: null;
        endTime: null;
        startTime: null;
    };
}

export interface RealtyAPIRooms {
    [key: number]: {
        description: string;
        features: string;
        features2: null;
        features3: null;
        length: string;
        width: string;
    };
}

export interface RealtyAPITimestamps {
    idxUpdated: null;
    listingUpdated: string;
    photosUpdated: string;
    conditionalExpiryDate: null;
    terminatedDate: null;
    suspendedDate: null;
    listingEntryDate: null;
    closedDate: null;
    unavailableDate: null;
    expiryDate: null;
    extensionEntryDate: null;
}

interface RealtyAPIAgentAddress {
    address1: string;
    address2: string;
    city: string;
    state: string;
    postal: string;
    country: string;
}

export interface RealtyAPIAgent {
    agentId: number;
    boardAgentId: string;
    updatedOn: string;
    name: string;
    board: string;
    position: string;
    phones: number[];
    social: string[];
    website: string;
    photo: {
        small: string;
        large: string;
        updatedOn: string;
    };
    brokerage: {
        name: string;
        address: RealtyAPIAgentAddress;
    };
}

export interface RealtyAPIResponseError {
    error: string;
}

export interface RealtyAPIListing {
    boardId: number;
    mlsNumber: string;
    status: string;
    class: string;
    type: string;
    listPrice: string;
    daysOnMarket: string;
    occupancy: string;
    listDate: string;
    updatedOn: string;
    lastStatus: string;
    soldPrice: string;
    soldDate: null;
    address: RealtyAPIAddress;
    condominium: RealtyAPICondominium;
    details: RealtyAPIDetails;
    lot: RealtyAPILot;
    map: RealtyAPIMap;
    nearby: {
        ammenities: null[];
    };
    office: {
        brokerageName: string;
    };
    openHouse: RealtyAPIOpenHouse;
    permissions: {
        displayAddressOnInternet: string;
        displayPublic: string;
    };
    rooms: RealtyAPIRooms;
    taxes: {
        annualAmount: null;
        assessmentYear: null;
    };
    timestamps: RealtyAPITimestamps;
    images: string[];
    agents: RealtyAPIAgent[];
    comparables?: RealtyAPIListing[];
}

export interface RealtyAPIQueryResponse {
    page: number;
    numPages: number;
    pageSize: number;
    count: number;
    statistics: {
        listPrice: {
            min: string;
            max: string;
        };
    };
    listings: RealtyAPIListing[];
}

export interface RealtyAPIAutosuggestionQueryParams {
    query: string;
}

export type LocationType = 'city' | 'neighborhood';

export interface RealtyAPIMixedNames {
    separatedNames: string[];
    type: LocationType | AutocompleteOptionType;
    cityName: string;
}

export interface RealtyAPILocations {
    boards: RealtyAPIBoard[];
}

export interface RealtyAPIBoard {
    boardId: number;
    name: string;
    updatedOn: string;
    classes: RealtyAPIBoardClass[];
}

export interface RealtyAPIBoardClass {
    name: string;
    areas: RealtyAPIBoardArea[];
}

export interface RealtyAPIBoardArea {
    name: string;
    cities: RealtyAPIBoardCity[];
}

export interface RealtyAPIBoardCity extends Partial<RealtyAPIMixedNames> {
    name: string;
    activeCount: number;
    location: {
        lat: number;
        lng: number;
    };
    neighborhoods?: RealtyAPINeighborhood[];
}

export interface RealtyAPINeighborhood extends Partial<RealtyAPIMixedNames> {
    name: string;
    activeCount: number;
    location: {
        lat: number;
        lng: number;
    };
}

export interface AutosuggestionGroupTitle {
    name: string;
    type: AutocompleteOptionType;
}

export interface AutosuggestionRecentSimpleOption extends Partial<RealtyAPIMixedNames> {
    name: string;
    location: {
        lat: number;
        lng: number;
    };
}

type AutocompleteViewTypeRecent = 'RECENT';

type AutocompleteViewTypeSearch = 'SEARCH';

export type AutocompleteViewType = AutocompleteViewTypeRecent | AutocompleteViewTypeSearch;

export type AutosuggestionSearchOption = RealtyAPIBoardCity | RealtyAPINeighborhood;

export type AutosuggestionRecentOption = AutosuggestionGroupTitle | AutosuggestionRecentSimpleOption;

export type AutocompleteOptionType = 'GroupTitle';

export interface RealtyAPILocationsByIp {
    current: {
        cityName?: string;
        location: {
            lat?: number;
            lng?: number;
        };
    };
    locations: (RealtyAPIBoardCity | RealtyAPINeighborhood)[];
}
