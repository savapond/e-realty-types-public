# Installation
 * Set up an SSH key (local + remote) - https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/ <br />
 * yarn add --save @types/e-realty-types`

# Summary
This package contains type definitions for:
* e-realty-back (https://bitbucket.org/omworks/e-realty-back/)
* e-realty-front (https://bitbucket.org/omworks/e-realty-front/)

# Details
Files were exported from https://bitbucket.org/omworks/e-realty-types/



